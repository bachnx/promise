// function Defer() {
//
// }
// Defer.prototype = {
//     promise: null,
//     status: 1,
//     deferCallback: [],
//     resolve: function (data) {
//         this.promise = new Promises(null);
//         setTimeout(function () {
//             temp = this.promise.resolveCallback.slice();
//             temp.reduce(function (result, next, index){
//                 // console.log(result);
//               if(result instanceof Promises) return null;
//               if(result === null) return null;
//               else
//               {
//                   this.promise.resolveCallback.shift();
//                   return next(result);
//               }
//             }, data);
//         }, 0)
//     },
//     reject: function (data) {
//         this.status = 0;
//
//     }
// }
//
// function Promises(deferCallback) {
//   if(deferCallback !== null){
//       this.execute(deferCallback);
//   }
// }
// Promises.prototype = {
//   resolveCallback: [],
//   rejectCallback: [],
//   then: function(resolve, reject) {
//     this.resolveCallback.push(resolve);
//     if (typeof reject !== 'unidentified') {
//       this.rejectCallback.push(reject);
//     }
//     // console.log(this.resolveCallback);
//     return this;
//   },
//   catch: function () {
//       this.rejectCallback.push(reject);
//   },
//   execute: function (deferCallback) {
//       var defer = new Defer();
//       defer.deferCallback.push(deferCallback);
//       if(defer.deferCallback.length > 0){
//           var shiftCallback = defer.deferCallback.shift();
//           shiftCallback(defer.resolve, defer.reject);
//       }
//       else {
//           this.resolveCallback.shift();
//       }
//   }
// }
// //
// var promise  = new Promises(function(resolve){
//   resolve(10);
// });
// promise.then(function(data){console.log(data);return data + 10;})
//     .then(function(data){
//       console.log(data);
//       return new Promises(function(resolve){
//           resolve(100);
//       });
//     })
//     .then(function(data){console.log(data); return data + 10;})
//     .then(function(data){console.log(data)});
//
// new Promises(function(resolve, reject) {
//     // A mock async action using setTimeout
//     setTimeout(function() { resolve(10); }, 1000);
// })
//     .then(function(num) { console.log('first then: ', num); return num * 2; })
//     .then(function(num) {
//         return new Promises(function(resolve, reject) {
//             // A mock async action using setTimeout
//             resolve(100);
//         })
//     })
//     .then(function(num) { console.log('second then: ', num); return num;}, function(num){ console.log('faillllll'); return 'faillllll';})
//     .then(function(num) { console.log('last then 2: ', num);}, function(){ console.log('fail2222');})
//     .catch(function(num){ console.log('fail: ',num+1);});













var Promises = function () {
    this.okCallbacks = [];
    this.koCallbacks = [];
};

Promises.prototype = {
    okCallbacks: null,
    koCallbacks: null,
    status: 'pending',
    error: null,

    then: function (okCallback, koCallback) {
        var defer = new Defer();

        // Add callbacks to the arrays with the defer binded to these callbacks
        this.okCallbacks.push({
            func: okCallback,
            defer: defer
        });

        if (koCallback) {
            this.koCallbacks.push({
                func: koCallback,
                defer: defer
            });
        }

        // Check if the promise is not pending. If not call the callback
        if (this.status === 'resolved') {
            this.executeCallback({
                func: okCallback,
                defer: defer
            }, this.data)
        } else if(this.status === 'rejected') {
            this.executeCallback({
                func: koCallback,
                defer: defer
            }, this.error)
        }

        return defer.promise;
    },

    //thuc thi ham callback
    executeCallback: function (callbackData, result) {
        window.setTimeout(function () {
            var res = callbackData.func(result);
            if (res instanceof Promises) {
                callbackData.defer.bind(res);
            } else {
                callbackData.defer.resolve(res);
            }
        }, 0);
    }
};
var Defer = function () {
    this.promise = new Promises();
};

Defer.prototype = {
    promise: null,
    resolve: function (data) {
        //goi den ham callback dang duoc luu trong promise thuoc ve defer nay
        //truyen vao tham so la tham truyen vao ham resolve
        //cap nhat trang thai cua promise do
        var promise = this.promise;
        promise.data = data;
        promise.status = 'resolved';
        promise.okCallbacks.forEach(function(callbackData) {
            promise.executeCallback(callbackData, data);
        });
    },

    reject: function (error) {
        var promise = this.promise;
        promise.error = error;
        promise.status = 'rejected';
        promise.koCallbacks.forEach(function(callbackData) {
            promise.executeCallback(callbackData, error);
        });
    },

    bind: function (promise) {
        var that = this;
        promise.then(function (res) {
            that.resolve(res);
        }, function (err) {
            that.reject(err);
        })
    }
};
//
// new Promises(function(resolve, reject) {
//     console.log(1231);
//     // A mock async action using setTimeout
//     setTimeout(function() { resolve(10); }, 1000);
// })
//     .then(function(num) { console.log('first then: ', num); return num * 2; })
//     .then(function(num) {
//         return new Promises(function(resolve, reject) {
//             // A mock async action using setTimeout
//             reject(10);
//         })
//     })
//     .then(function(num) { console.log('second then: ', num); return num;}
//         , function(num){ console.log('faillllll'); return 'faillllll';}
//     )
//     .then(function(num) { console.log('last then 2: ', num); return num;}, function(){ console.log('fail2222');})
//     .then(function(num) { console.log('sau then 2: ', num);});




function before() {
    var defer = new Defer();
    // an example of an async call
    setTimeout(function () {
        var random_boolean = 1 >= 0.5;
        if (random_boolean) {
            defer.resolve('success before');
        } else {
            defer.reject(new Error('error'));
        }
    }, 100);
    return defer.promise;
}

function after() {
    var defer = new Defer();
    // an example of an async call
    setTimeout(function () {
        var random_boolean = 1 >= 0.5;
        if (random_boolean) {
            defer.resolve('success after');
        } else {
            defer.reject(new Error('error'));
        }
    }, 100);
    return defer.promise;
}

before().then(function (text) {
    console.log(text);
    // Thay vì kết thúc luôn, trả về 1 Promise tiếp
    return after();
}, function (error) {
    console.log(error.message);
}).then(function (text) {
    console.log(text)
});